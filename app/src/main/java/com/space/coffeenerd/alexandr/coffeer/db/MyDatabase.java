package com.space.coffeenerd.alexandr.coffeer.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by alexandr on 01.04.15.
 */
public class MyDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "stories.db";
    private static final int DATABASE_VERSION = 1;
    public static final String STORIES_TABLE = "story";
    public static final String ADVICES_TABLE = "advice";
    public static final String RECIPES_TABLE = "recipe";

    private StoriesTable storiesTable;
    private AdvicesTable advicesTable;
    private RecipesTable recipesTable;

    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = getWritableDatabase();
        storiesTable = new StoriesTable(db);
        advicesTable = new AdvicesTable(db);
        recipesTable = new RecipesTable(db);
    }

    public RecipesTable getRecipesTable() {
        return recipesTable;
    }

    public StoriesTable getStoriesTable() {
        return storiesTable;
    }

    public AdvicesTable getAdvicesTable() {
        return advicesTable;
    }


}
