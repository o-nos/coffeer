package com.space.coffeenerd.alexandr.coffeer.activity;

import android.app.Activity;
import android.os.Bundle;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.fragments.StoryListFragment;

/**
 * Created by alexandr on 31.03.15.
 */
public class StoryListActivity extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story_list_activity);

        getFragmentManager().beginTransaction()
                .add(R.id.story_list_holder, new StoryListFragment())
                .commit();

    }

}
