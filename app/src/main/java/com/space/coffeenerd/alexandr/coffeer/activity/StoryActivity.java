package com.space.coffeenerd.alexandr.coffeer.activity;

import android.app.Activity;
import android.os.Bundle;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.fragments.ExtendedStoryFragment;

/**
 * Created by alexandr on 31.03.15.
 */
public class StoryActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        long storyId = getIntent()
                .getLongExtra(ExtendedStoryFragment.EXTRA_STORY_ID, 1);

        setContentView(R.layout.story_activity);

        getFragmentManager().beginTransaction()
                .add(R.id.story_fragment_holder, ExtendedStoryFragment.newInstance(storyId))
                .commit();

    }


}
