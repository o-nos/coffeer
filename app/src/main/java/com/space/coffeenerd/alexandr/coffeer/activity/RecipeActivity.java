package com.space.coffeenerd.alexandr.coffeer.activity;


import android.app.Activity;
import android.os.Bundle;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.fragments.ExtendedRecipeFragment;

/**
 * Created by alexandr on 28.03.15.
 */
public class RecipeActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        long recipeId = getIntent()
                .getLongExtra(ExtendedRecipeFragment.EXTRA_RECIPE_ID, 1);

        setContentView(R.layout.recipe_activity);

        getFragmentManager().beginTransaction()
                    .add(R.id.recipe_fragment_holder, ExtendedRecipeFragment.newInstance(recipeId))
                    .commit();

    }

}
