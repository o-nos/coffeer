package com.space.coffeenerd.alexandr.coffeer.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.space.coffeenerd.alexandr.coffeer.App;
import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.adapters.CoffeeRecipeAdapter;
import com.space.coffeenerd.alexandr.coffeer.models.CoffeeRecipe;

import java.util.ArrayList;

/**
 * Created by alexandr on 03.05.15.
 */
public class FavoriteListFragment extends RecipeListFragment {

    private CoffeeRecipeAdapter coffeeRecipeAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.recipes_button);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new FavoritesRecipesFetch().execute();
    }

    @Override
    public void onUpdate() {
        new FavoritesRecipesFetch().execute();
    }


    private class FavoritesRecipesFetch extends AsyncTask<Void, Void, ArrayList<CoffeeRecipe>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listView.setVisibility(View.GONE);
            mAdView.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<CoffeeRecipe> doInBackground(Void... params) {

            App.getCacheManager().setFavoriteRecipes(App.getDatabase().getRecipesTable().getFavoriteRecipes());

            return App.getCacheManager().getFavoriteRecipes();
        }

        @Override
        protected void onPostExecute(ArrayList<CoffeeRecipe> coffeeRecipes) {
            coffeeRecipeArrayList = coffeeRecipes;

            coffeeRecipeAdapter = new CoffeeRecipeAdapter(getActivity(), coffeeRecipeArrayList);

            listView.setAdapter(coffeeRecipeAdapter);

            progress.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);

            if (coffeeRecipes.isEmpty()) {
                mAdView.setVisibility(View.GONE);
            } else {
                mAdView.setVisibility(View.VISIBLE);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            }

        }
    }

}
