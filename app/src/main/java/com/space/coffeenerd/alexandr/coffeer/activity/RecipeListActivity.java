package com.space.coffeenerd.alexandr.coffeer.activity;


import android.os.Bundle;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.fragments.RecipeListFragment;

/**
 * Created by Alexandr on 20/03/2015.
 */
public class RecipeListActivity extends BaseListActivity {

    private RecipeListFragment recipeListFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_list_activity);

        getFragmentManager().beginTransaction()
                 .add(R.id.recipe_list_holder, recipeListFragment = new RecipeListFragment())
                 .commit();

    }


    @Override
    public void onFragmentUpdate() {
        if (recipeListFragment != null && recipeListFragment.isAdded()) {
            recipeListFragment.onUpdate();
        }
    }
}
