package com.space.coffeenerd.alexandr.coffeer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.models.Ingredient;

import java.util.ArrayList;

/**
 * Created by Alexandr on 14/04/2015.
 */
public class IngredientsAdapter extends ArrayAdapter<Ingredient> {

    public IngredientsAdapter(Context context, ArrayList<Ingredient> ingredientArrayList) {
        super(context, 0, ingredientArrayList);
    }

    static class ViewHolder {
        public TextView title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        // якщо ще не отримали представлення, то заповнюємо його
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.ingredient_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.ingred_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Ingredient ingredient = getItem(position); // налаштування кожного окремого об’єкта

        viewHolder.title.setText(ingredient.getTitle());

        return convertView;
    }


}
