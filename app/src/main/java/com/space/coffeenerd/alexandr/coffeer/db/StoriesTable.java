package com.space.coffeenerd.alexandr.coffeer.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.space.coffeenerd.alexandr.coffeer.models.Story;

import java.util.ArrayList;

/**
 * Created by Alexandr on 01/04/2015.
 */
public class StoriesTable extends AbstractTable {

    private static final String S_ID = "_id";
    private static final String S_TITLE = "title";
    private static final String S_ARTICLE = "article";
    private static final String S_PICTURE = "picture";

    private static final String S_SELECT_ALL_FROM = "select * from ";
    private static final String S_WHERE_ID = " where _id = ?";

    public StoriesTable(SQLiteDatabase db) {
        super(db);
    }

    public ArrayList<Story> getStories() {
        ArrayList<Story> stories = new ArrayList<Story>();

        Cursor cursor = db.query(MyDatabase.STORIES_TABLE, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                Story story = cursorForStoryList(cursor);
                stories.add(story);
            } while (cursor.moveToNext());
        }

        // close the cursor!
        cursor.close();

        return stories;
    }

    private Story cursorForStoryList(Cursor cursor) {

        Story story = new Story();

        story.setId(cursor.getInt(cursor.getColumnIndex(S_ID)));
        story.setStoryTitle(cursor.getString(cursor.getColumnIndex(S_TITLE)));
        story.setPicture(cursor.getString(cursor.getColumnIndex(S_PICTURE)));

        return story;
    }


    private Story cursorToStory(Cursor cursor) {

        Story story = new Story();

        story.setId(cursor.getInt(cursor.getColumnIndex(S_ID)));
        story.setStoryTitle(cursor.getString(cursor.getColumnIndex(S_TITLE)));
        story.setArticle(cursor.getString(cursor.getColumnIndex(S_ARTICLE)));
        story.setPicture(cursor.getString(cursor.getColumnIndex(S_PICTURE)));

        return story;
    }


    public Story getStoryFromDataBase(long id) {

        Cursor cursor = db.rawQuery(S_SELECT_ALL_FROM + MyDatabase.STORIES_TABLE +
               S_WHERE_ID, new String[]{Long.toString(id)});

        Story story = null;
        if (cursor.moveToFirst()) {
                story = cursorToStory(cursor);
                return story;
        }

        cursor.close();

        return story;
    }

}
