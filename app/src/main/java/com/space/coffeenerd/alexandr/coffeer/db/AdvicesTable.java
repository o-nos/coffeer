package com.space.coffeenerd.alexandr.coffeer.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.space.coffeenerd.alexandr.coffeer.models.Advice;

import java.util.ArrayList;


/**
 * Created by Alexandr on 13/04/2015.
 */
public class AdvicesTable extends AbstractTable {

    private static final String S_ID = "_id";
    private static final String S_TITLE = "title";
    private static final String S_PHOTO = "photo";
    private static final String S_TEXT = "text";

    private static final String S_SELECT_ALL_FROM = "select * from ";
    private static final String S_WHERE_ID = " where _id = ?";

    public AdvicesTable(SQLiteDatabase db) {
        super(db);
    }

    public ArrayList<Advice> getAdvices() {
        ArrayList<Advice> advices = new ArrayList<Advice>();

        Cursor cursor = db.query(MyDatabase.ADVICES_TABLE, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                Advice advice = cursorForAdviceList(cursor);
                advices.add(advice);
            } while (cursor.moveToNext());
        }

        // close the cursor!
        cursor.close();

        return advices;
    }

    private Advice cursorForAdviceList(Cursor cursor) {

        Advice advice = new Advice();

        advice.setId(cursor.getInt(cursor.getColumnIndex(S_ID)));
        advice.setTitle(cursor.getString(cursor.getColumnIndex(S_TITLE)));

        return advice;
    }

    private Advice cursorToAdvice(Cursor cursor) {

        Advice advice = new Advice();

        advice.setId(cursor.getInt(cursor.getColumnIndex(S_ID)));
        advice.setTitle(cursor.getString(cursor.getColumnIndex(S_TITLE)));
        advice.setPhoto(cursor.getString(cursor.getColumnIndex(S_PHOTO)));
        advice.setAdviceText(cursor.getString(cursor.getColumnIndex(S_TEXT)));

        return advice;
    }

    public Advice getAdviceFromDataBase(long id) {

        Cursor cursor = db.rawQuery(S_SELECT_ALL_FROM + MyDatabase.ADVICES_TABLE + S_WHERE_ID,
                new String[]{Long.toString(id)});

        Advice advice = null;
        if (cursor.moveToFirst()) {
                advice = cursorToAdvice(cursor);
                return advice;
        }
        cursor.close();

        return advice;
    }


}
