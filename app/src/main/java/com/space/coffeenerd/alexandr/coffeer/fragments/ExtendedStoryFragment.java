package com.space.coffeenerd.alexandr.coffeer.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.plus.PlusShare;
import com.space.coffeenerd.alexandr.coffeer.App;
import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.models.Story;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.MalformedURLException;
import java.net.URL;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by alexandr on 31.03.15.
 */
public class ExtendedStoryFragment extends Fragment {

    public static final String EXTRA_STORY_ID = "STORY_ID";

    private Story currentStory;
    @InjectView(R.id.story_title)
    TextView rTitle;
    @InjectView(R.id.story_picture1)
    ImageView rPic1;
    @InjectView(R.id.story_article1)
    TextView rArticle;

    CallbackManager callbackManager;
    ShareDialog shareDialog;
    FloatingActionButton facebookButton;
    FloatingActionButton twitterButton;
    FloatingActionButton googleButton;

    // why has it be static ?
    public static ExtendedStoryFragment newInstance(long storyID) {

        Bundle args = new Bundle();
        args.putLong(EXTRA_STORY_ID, storyID);

        ExtendedStoryFragment fragment = new ExtendedStoryFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(getActivity());

        long storyId = getArguments().getLong(EXTRA_STORY_ID);

        currentStory = App.getDatabase().getStoriesTable().getStoryFromDataBase(storyId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.extended_story_fragment, container, false);

        ButterKnife.inject(this, v);

        facebookButton = (FloatingActionButton) v.findViewById(R.id.facebook_share_button);
        facebookButton.setIcon(R.drawable.facebook_icon);

        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle(getString(R.string.facebook_share_title))
                            .setContentDescription(getString(R.string.facebook_content_description))
                            .setContentUrl(Uri.parse(getString(R.string.share_url)))
                            .setImageUrl(Uri.parse(getString(R.string.facebook_share_image)))
                            .build();
                    shareDialog.show(linkContent);
                }
            }
        });

        twitterButton = (FloatingActionButton) v.findViewById(R.id.twitter_share_button);
        twitterButton.setIcon(R.drawable.twitter_icon);

        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                URL url = null;
                try {
                    url = new URL(getString(R.string.share_url));
                } catch (MalformedURLException e) {
                    Toast.makeText(getActivity(), getString(R.string.twitter_exception), Toast.LENGTH_SHORT).show();
                }

                TweetComposer.Builder builder = new TweetComposer.Builder(getActivity())
                        .text(getString(R.string.twitter_share_text))
                        .url(url);

                builder.show();

            }
        });


        googleButton = (FloatingActionButton) v.findViewById(R.id.google_share_button);
        googleButton.setIcon(R.drawable.google_icon);

        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Launch the Google+ share dialog with attribution to your app.
                Intent shareIntent = new PlusShare.Builder(getActivity())
                        .setType(getString(R.string.google_share_type))
                        .setText(getString(R.string.google_share_message))
                        .setContentUrl(Uri.parse(getString(R.string.share_url)))
                        .getIntent();

                startActivityForResult(shareIntent, 0);

            }
        });


        Picasso.with(getActivity())
                .load(currentStory.getPicture1())
                .placeholder(R.drawable.cofee_plchldr_5)
                .error(R.drawable.cofee_plchldr_5)
                .into(rPic1);

        rTitle.setText(currentStory.getStoryTitle());
        rArticle.setText(currentStory.getArticle());

        return v;
    }

    // to handle the response
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
