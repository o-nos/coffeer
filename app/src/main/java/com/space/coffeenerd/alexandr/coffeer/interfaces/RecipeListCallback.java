package com.space.coffeenerd.alexandr.coffeer.interfaces;

import com.space.coffeenerd.alexandr.coffeer.models.CoffeeRecipe;

/**
 * Created by alexandr on 21.05.15.
 */
public interface RecipeListCallback {

    void onRecipeClick(CoffeeRecipe recipe);

}
