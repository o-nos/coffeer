package com.space.coffeenerd.alexandr.coffeer.activity;

import android.app.Activity;
import android.os.Bundle;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.fragments.ExtendedAdviceFragment;

/**
 * Created by Alexandr on 13/04/2015.
 */
public class AdviceActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        long adviceId = getIntent()
                .getLongExtra(ExtendedAdviceFragment.EXTRA_ADVICE_ID, 1);

        setContentView(R.layout.advice_activity);

        getFragmentManager().beginTransaction()
                .add(R.id.advice_fragment_holder, ExtendedAdviceFragment.newInstance(adviceId))
                .commit();

    }

}
