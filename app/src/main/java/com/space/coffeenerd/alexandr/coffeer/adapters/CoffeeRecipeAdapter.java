package com.space.coffeenerd.alexandr.coffeer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.models.CoffeeRecipe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by alexandr on 28.03.15.
 *
 * 8.5. Example at:
 * http://www.vogella.com/tutorials/AndroidListView/article.html#adapterperformance_holder
 *
 */
public class CoffeeRecipeAdapter extends ArrayAdapter<CoffeeRecipe> {

    public CoffeeRecipeAdapter(Context context, ArrayList<CoffeeRecipe> coffeeList) {
        super(context,0,coffeeList);
    }

    static class ViewHolder {
        public TextView name;
        public ImageView photo;
        public TextView about;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.recipe_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView)convertView.findViewById(R.id.recipe_name);
            viewHolder.about = (TextView)convertView.findViewById(R.id.recipe_about);
            viewHolder.photo = (ImageView)convertView.findViewById(R.id.recipe_photo);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CoffeeRecipe recipe = getItem(position);

        viewHolder.name.setText(recipe.getName());

        viewHolder.about.setText(recipe.getAbout());

        // nice situation with AsyncTask. Can this app be crushed here if there is no Internet connection?
        Picasso.with(getContext())
                .load(recipe.getStringUrl())
                .placeholder(R.drawable.coffee_plchldr_5)
                .error(R.drawable.coffee_plchldr_5)
                .resize(100, 100)
                .centerCrop()
                .into(viewHolder.photo);

//        viewHolder.photo.setImageResource(R.drawable.tumblr); // for debug

        return convertView;
    }



}
