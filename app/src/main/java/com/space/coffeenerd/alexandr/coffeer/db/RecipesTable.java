package com.space.coffeenerd.alexandr.coffeer.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.space.coffeenerd.alexandr.coffeer.App;
import com.space.coffeenerd.alexandr.coffeer.models.CoffeeRecipe;

import java.util.ArrayList;

/**
 * Created by Alexandr on 23/04/2015.
 */
public class RecipesTable extends AbstractTable {

    private final static String S_ID = "_id";
    private final static String S_NAME = "name";
    private final static String S_ABOUT = "about";
    private final static String S_DESCRIPTION = "description";
    private final static String S_PHOTO = "photo";
    private final static String S_PREP = "prep";
    private final static String S_SERVES = "serves";
    private final static String S_HOW = "how";
    private final static String S_INGREDIENTS = "ingredients";
    private final static String S_FAV = "fav";

    private static final String S_SELECT_ALL_FROM = "select * from ";
    private static final String S_WHERE_ID = " where _id = ?";
    private static final String S_WHERE_FAV = " where fav = ?";

    public RecipesTable(SQLiteDatabase db) {
        super(db);
    }

    public ArrayList<CoffeeRecipe> getRecipes() {
        ArrayList<CoffeeRecipe> recipes = new ArrayList<>();

        Cursor cursor = db.query(MyDatabase.RECIPES_TABLE, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                CoffeeRecipe recipe = cursorForRecipeList(cursor);
                recipes.add(recipe);
            } while (cursor.moveToNext());
        }

        // close the cursor!
        cursor.close();

        return recipes;
    }

    public ArrayList<CoffeeRecipe> getFavoriteRecipes() {
        ArrayList<CoffeeRecipe> recipes = new ArrayList<>();

        Cursor cursor = db.rawQuery(S_SELECT_ALL_FROM + MyDatabase.RECIPES_TABLE
                + S_WHERE_FAV, new String[]{String.valueOf(1)}); // can I put here just "1" as parameter?

        if (cursor.moveToFirst()) {
            do {
                CoffeeRecipe recipe = cursorForRecipeList(cursor);
                recipes.add(recipe);
            } while (cursor.moveToNext());
        }

        // close the cursor!
        cursor.close();

        return recipes;
    }

    private CoffeeRecipe cursorForRecipeList(Cursor cursor){

        CoffeeRecipe recipe = new CoffeeRecipe();

        recipe.setId(cursor.getInt(cursor.getColumnIndex(S_ID)));
        recipe.setName(cursor.getString(cursor.getColumnIndex(S_NAME)));
        recipe.setAbout(cursor.getString(cursor.getColumnIndex(S_ABOUT)));
        recipe.setStringUrl(cursor.getString(cursor.getColumnIndex(S_PHOTO)));

        return recipe;
    }

    private CoffeeRecipe cursorToRecipe(Cursor cursor) {

        CoffeeRecipe recipe = new CoffeeRecipe();

        recipe.setId(cursor.getInt(cursor.getColumnIndex(S_ID)));
        recipe.setName(cursor.getString(cursor.getColumnIndex(S_NAME)));
        recipe.setAbout(cursor.getString(cursor.getColumnIndex(S_ABOUT)));
        recipe.setStringUrl(cursor.getString(cursor.getColumnIndex(S_PHOTO)));
        recipe.setDescription(cursor.getString(cursor.getColumnIndex(S_DESCRIPTION)));
        recipe.setPrepTime(cursor.getInt(cursor.getColumnIndex(S_PREP)));
        recipe.setCups(cursor.getInt(cursor.getColumnIndex(S_SERVES)));
        recipe.setHowToMake(cursor.getString(cursor.getColumnIndex(S_HOW)));
        recipe.setIngredients(cursor.getString(cursor.getColumnIndex(S_INGREDIENTS)));
        recipe.setFavorite(cursor.getInt(cursor.getColumnIndex(S_FAV)) == 1);

        return recipe;
    }

    public CoffeeRecipe getRecipeFromDataBase(long id) {

        Cursor cursor = db.rawQuery(S_SELECT_ALL_FROM + MyDatabase.RECIPES_TABLE
                + S_WHERE_ID, new String[]{Long.toString(id)});

        CoffeeRecipe recipe = null;
        if (cursor.moveToFirst()) {
            recipe = cursorToRecipe(cursor);
        }
        cursor.close();

        return recipe;

    }

    public void addRecipeUpdate(CoffeeRecipe currentRecipe){

        SQLiteDatabase db = App.getDatabase().getReadableDatabase();

        // New value for one column
        ContentValues dataToInsert = new ContentValues();
        dataToInsert.put(S_FAV, "1");

        // Which row to update, based on the ID
        String where = "_id=?";
        String[] whereArgs = new String[]{String.valueOf(currentRecipe.getId())};

        db.update(MyDatabase.RECIPES_TABLE, dataToInsert, where, whereArgs);

    }

    public void removeRecipeUpdate(CoffeeRecipe currentRecipe){

        SQLiteDatabase db = App.getDatabase().getReadableDatabase();

        // New value for one column
        ContentValues dataToInsert = new ContentValues();
        dataToInsert.put(S_FAV, "0");

        // Which row to update, based on the ID
        String where = "_id=?";
        String[] whereArgs = new String[]{String.valueOf(currentRecipe.getId())};

        db.update(MyDatabase.RECIPES_TABLE, dataToInsert, where, whereArgs);
    }

}
