package com.space.coffeenerd.alexandr.coffeer.models;

/**
 * Created by alexandr on 30.03.15.
 */
public class Story {

    public long id;

    public String storyTitle;
    public String article1;
    public String picture1;

    public Story() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStoryTitle() {
        return storyTitle;
    }

    public void setStoryTitle(String storyTitle) {
        this.storyTitle = storyTitle;
    }

    public String getArticle() {
        return article1;
    }

    public void setArticle(String article1) {
        this.article1 = article1;
    }

    public String getPicture1() {
        return picture1;
    }

    public void setPicture(String picture1) {
        this.picture1 = picture1;
    }

}
