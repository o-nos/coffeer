package com.space.coffeenerd.alexandr.coffeer.models;

/**
 * Created by Alexandr on 13/04/2015.
 */
public class Advice {

    public long id;

    public String title;
    public String photo;
    public String adviceText;

    public Advice(){

    }

    public Advice(long id, String title, String photo, String adviceText) {
        this.id = id;
        this.title = title;
        this.photo = photo;
        this.adviceText = adviceText;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAdviceText() {
        return adviceText;
    }

    public void setAdviceText(String adviceText) {
        this.adviceText = adviceText;
    }
}
