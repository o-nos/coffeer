package com.space.coffeenerd.alexandr.coffeer.activity;

import android.app.Activity;
import android.content.Intent;

import com.space.coffeenerd.alexandr.coffeer.fragments.ExtendedRecipeFragment;
import com.space.coffeenerd.alexandr.coffeer.interfaces.RecipeListCallback;
import com.space.coffeenerd.alexandr.coffeer.models.CoffeeRecipe;

/**
 * Created by alexandr on 21.05.15.
 */
public abstract class BaseListActivity extends Activity implements RecipeListCallback {

    private static final int REQUEST_EXTENDED = 2313;

    @Override
    public void onRecipeClick(CoffeeRecipe recipe) {
        Intent i = new Intent(this, RecipeActivity.class);
        i.putExtra(ExtendedRecipeFragment.EXTRA_RECIPE_ID, recipe.getId());
        startActivityForResult(i, REQUEST_EXTENDED);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_EXTENDED && resultCode == Activity.RESULT_OK) {
            onFragmentUpdate();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public abstract void onFragmentUpdate();
}
