package com.space.coffeenerd.alexandr.coffeer.models;

/**
 * Created by Alexandr on 14/04/2015.
 */
public class Ingredient {

    public String title;

    public String getTitle() {
        return title;
    }

//    public void setTitle(String title) {
//        this.title = title;
//    }

    public Ingredient(String title) {
        this.title = title;
    }
}
