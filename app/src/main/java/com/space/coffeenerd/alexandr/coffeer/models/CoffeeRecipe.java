package com.space.coffeenerd.alexandr.coffeer.models;

/**
 * Created by alexandr on 21.03.15.
 */
public class CoffeeRecipe {

    private long id;
    private String name;
    private String stringUrl;
    private String about; // short description to be shown in list
    private String description; // to be shown in extended fragment
    private int prepTime;

    private int cups; //serves or yield
    private String ingredients;
    private String howToMake;
    private boolean favorite;

    public CoffeeRecipe(long id, String name, String stringUrl,
                        String about, String description,
                        int prepTime, int cups, String ingredients, String howToMake, boolean favorite) {
        this.id = id;
        this.name = name;
        this.stringUrl = stringUrl;
        this.about = about;
        this.description = description;
        this.prepTime = prepTime;
        this.cups = cups;
        this.ingredients = ingredients;
        this.favorite = favorite;
        this.howToMake = howToMake;
    }

    public CoffeeRecipe() {
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override

    public String toString() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStringUrl() {
        return stringUrl;
    }

    public void setStringUrl(String stringUrl) {
        this.stringUrl = stringUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCups() {
        return cups;
    }

    public void setCups(int cups) {
        this.cups = cups;
    }

    public String getHowToMake() {
        return howToMake;
    }

    public void setHowToMake(String howToMake) {
        this.howToMake = howToMake;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public int getPrepTime() {
        return prepTime;
    }

    public void setPrepTime(int prepTime) {
        this.prepTime = prepTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }


}
