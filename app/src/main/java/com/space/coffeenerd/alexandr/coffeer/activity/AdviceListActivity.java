package com.space.coffeenerd.alexandr.coffeer.activity;

import android.app.Activity;
import android.os.Bundle;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.fragments.AdviceListFragment;

/**
 * Created by Alexandr on 13/04/2015.
 */
public class AdviceListActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advice_list_activity);

        getFragmentManager().beginTransaction()
                .add(R.id.advice_list_holder, new AdviceListFragment())
                .commit();

    }
}
