package com.space.coffeenerd.alexandr.coffeer.fragments;


import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.space.coffeenerd.alexandr.coffeer.App;
import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.activity.AdviceActivity;
import com.space.coffeenerd.alexandr.coffeer.adapters.AdviceAdapter;
import com.space.coffeenerd.alexandr.coffeer.models.Advice;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Alexandr on 13/04/2015.
 */
public class AdviceListFragment extends Fragment {

    private ArrayList<Advice> adviceList;

    @InjectView(R.id.advice_list_view)
    ListView listView;

    @InjectView(android.R.id.progress)
    ProgressBar progress;

    @InjectView(R.id.adView_advice)
    AdView mAdView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.advices_button);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.advice_fragment, container, false);

        ButterKnife.inject(this, rootView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Advice advice = (Advice) listView.getItemAtPosition(position);

                Intent i = new Intent(getActivity(), AdviceActivity.class);
                i.putExtra(ExtendedAdviceFragment.EXTRA_ADVICE_ID, advice.getId());

                startActivity(i);
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new AdviceFetch().execute();
    }


    private class AdviceFetch extends AsyncTask<Void, Void, ArrayList<Advice>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mAdView.setVisibility(View.GONE);
            listView.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Advice> doInBackground(Void... params) {

            return App.getDatabase().getAdvicesTable().getAdvices();
        }

        @Override
        protected void onPostExecute(ArrayList<Advice> advices) {
            adviceList = advices;

            listView.setAdapter(new AdviceAdapter(getActivity(), adviceList));

            progress.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            mAdView.setVisibility(View.VISIBLE);

            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }
    }


}
