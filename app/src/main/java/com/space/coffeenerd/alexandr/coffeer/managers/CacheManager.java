package com.space.coffeenerd.alexandr.coffeer.managers;

import com.space.coffeenerd.alexandr.coffeer.models.CoffeeRecipe;

import java.util.ArrayList;

/**
 * Created by alexandr on 21.05.15.
 */
public class CacheManager {

    public ArrayList<CoffeeRecipe> list;

    public CoffeeRecipe getRecipe(long id) {
        for (CoffeeRecipe c : list) {
            if (c.getId() == id)
                return c;
        }
        return null;
    }

    public ArrayList<CoffeeRecipe> getFavoriteRecipes() {
        if (list == null){
            list = new ArrayList<>();
        }
        return list;
    }

    public void setFavoriteRecipes(ArrayList<CoffeeRecipe> list) {
        this.list = list;
    }
}
