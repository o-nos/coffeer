package com.space.coffeenerd.alexandr.coffeer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.models.Advice;

import java.util.ArrayList;

/**
 * Created by Alexandr on 13/04/2015.
 */
public class AdviceAdapter extends ArrayAdapter<Advice> {

    public AdviceAdapter(Context context, ArrayList<Advice> adviceList) {
        super(context, 0, adviceList);
    }

    static class ViewHolder {
        public TextView title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.advice_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.advice_title_view);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // create each list item
        Advice advice = getItem(position);

        viewHolder.title.setText(advice.getTitle());

        return convertView;
    }
}
