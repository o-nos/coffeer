package com.space.coffeenerd.alexandr.coffeer.activity;

import android.os.Bundle;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.fragments.FavoriteListFragment;

/**
 * Created by alexandr on 03.05.15.
 */
public class FavoritesListActivity extends BaseListActivity {

    private FavoriteListFragment listFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorite_activity);

        getFragmentManager().beginTransaction()
                .add(R.id.favorite_list_fragment, listFragment = new FavoriteListFragment())
                .commit();
    }

    @Override
    public void onFragmentUpdate() {
        if (listFragment != null && listFragment.isAdded()) {
            listFragment.onUpdate();
        }
    }
}
