package com.space.coffeenerd.alexandr.coffeer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.models.Story;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by alexandr on 31.03.15.
 */
public class StoryAdapter extends ArrayAdapter<Story> {

    public StoryAdapter(Context context, ArrayList<Story> storyList) {
        super(context, 0, storyList);
    }

    static class ViewHolder {
        public TextView name;
        public ImageView photo;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        // якщо ще не отримали представлення, то заповнюємо його
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.story_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.story_name_list);
            viewHolder.photo = (ImageView) convertView.findViewById(R.id.story_photo_list);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Story story = getItem(position); // налаштування кожного окремого об’єкта

        viewHolder.name.setText(story.getStoryTitle());

        Picasso.with(getContext())
                .load(story.getPicture1())
                .placeholder(R.drawable.coffee_plchldr_3)
                .error(R.drawable.coffee_plchldr_3)
                .into(viewHolder.photo);

        return convertView;
    }
}
