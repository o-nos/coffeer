package com.space.coffeenerd.alexandr.coffeer.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.space.coffeenerd.alexandr.coffeer.App;
import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.adapters.IngredientsAdapter;
import com.space.coffeenerd.alexandr.coffeer.models.CoffeeRecipe;
import com.space.coffeenerd.alexandr.coffeer.models.Ingredient;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ExtendedRecipeFragment extends Fragment {

    public static final String EXTRA_RECIPE_ID = "RECIPE_ID";

    CoffeeRecipe currentRecipe;

    @InjectView(R.id.rec_name)
    TextView rName;
    @InjectView(R.id.rec_recipe_photo)
    ImageView rPhoto;
    @InjectView(R.id.rec_description)
    TextView rDescription;
    @InjectView(R.id.rec_prep_time)
    TextView rPrepTime;
    @InjectView(R.id.rec_serves_int)
    TextView rServesIntView;
    FloatingActionButton rFavoriteButton;

    ListView ingredsList;

    TextView rHowTo;

    public static ExtendedRecipeFragment newInstance(long recipeID) {

        Bundle args = new Bundle();
        args.putLong(EXTRA_RECIPE_ID, recipeID);

        ExtendedRecipeFragment fragment = new ExtendedRecipeFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        long recipeId = getArguments().getLong(EXTRA_RECIPE_ID);

        currentRecipe = App.getDatabase().getRecipesTable().getRecipeFromDataBase(recipeId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.extended_recipe_fragment, container, false);

        ingredsList = (ListView) v.findViewById(R.id.ingreds_list);


        View header = inflater.inflate(R.layout.recipe_header_layout, null, false);

        ButterKnife.inject(this, header);

        rName.setText(currentRecipe.getName());

        Picasso.with(getActivity())
                .load(currentRecipe.getStringUrl())
                .placeholder(R.drawable.cofee_plchldr_5)
                .error(R.drawable.cofee_plchldr_5)
                .into(rPhoto);

        rFavoriteButton = (FloatingActionButton) header.findViewById(R.id.add_favorite_button);
        rFavoriteButton.setSize(FloatingActionButton.SIZE_NORMAL);

        if (currentRecipe.isFavorite()) {
            rFavoriteButton.setIcon(R.drawable.ic_fab_ok);
        } else {
            rFavoriteButton.setIcon(R.drawable.ic_fab_star);
        }

        rFavoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentRecipe.isFavorite()) {

                    rFavoriteButton.setIcon(R.drawable.ic_fab_star);
                    removeFromFavorite();
                    Toast.makeText(getActivity(), getString(R.string.removed), Toast.LENGTH_SHORT).show();

                } else {

                    rFavoriteButton.setIcon(R.drawable.ic_fab_ok);
                    addToFavorite();
                    Toast.makeText(getActivity(), getString(R.string.added), Toast.LENGTH_SHORT).show();
                }
                getActivity().setResult(Activity.RESULT_OK);
            }
        });

        rDescription.setText(currentRecipe.getDescription());
        rPrepTime.setText(getString(R.string.prepare) + " " + Integer.toString(currentRecipe.getPrepTime()) + " " + getString(R.string.min));
        rServesIntView.setText(getString(R.string.serves) + " " + Integer.toString(currentRecipe.getCups()) + " " + getString(R.string.cup));

        View footer = inflater.inflate(R.layout.recipe_footer_layout, null, false);

        rHowTo = (TextView) footer.findViewById(R.id.rec_how_to_make);
        rHowTo.setText(currentRecipe.getHowToMake());

        Gson gson = new Gson();

        Type type = new TypeToken<ArrayList<Ingredient>>() {
        }.getType();
        ArrayList<Ingredient> ingredients = gson.fromJson(currentRecipe.getIngredients(), type);

        ingredsList.addHeaderView(header, null, false);
        ingredsList.addFooterView(footer, null, false);

        ingredsList.setAdapter(new IngredientsAdapter(getActivity(),
                ingredients));


        return v;
    }


    private void removeFromFavorite() {

        currentRecipe.setFavorite(false);

        App.getCacheManager().getFavoriteRecipes().remove(currentRecipe);

        App.getDatabase().getRecipesTable().removeRecipeUpdate(currentRecipe);

    }


    private void addToFavorite() {

        currentRecipe.setFavorite(true);

        App.getCacheManager().getFavoriteRecipes().add(currentRecipe);

        App.getDatabase().getRecipesTable().addRecipeUpdate(currentRecipe);
    }
}
