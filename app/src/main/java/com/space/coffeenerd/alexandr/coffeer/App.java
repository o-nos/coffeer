package com.space.coffeenerd.alexandr.coffeer;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.space.coffeenerd.alexandr.coffeer.db.MyDatabase;
import com.space.coffeenerd.alexandr.coffeer.managers.CacheManager;


/**
 * Created by Alexandr on 01/04/2015.
 */
public class App extends MultiDexApplication {

    private static MyDatabase database;
    private static CacheManager cacheManager = new CacheManager();

    @Override
    public void onCreate() {
        super.onCreate();
        database = new MyDatabase(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
//        Fabric.with(this, new TweetComposer());

    }

    public static MyDatabase getDatabase() {
        return database;
    }

    public static CacheManager getCacheManager() {
        return cacheManager;
    }
}
