package com.space.coffeenerd.alexandr.coffeer.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.space.coffeenerd.alexandr.coffeer.App;
import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.activity.StoryActivity;
import com.space.coffeenerd.alexandr.coffeer.adapters.StoryAdapter;
import com.space.coffeenerd.alexandr.coffeer.models.Story;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by alexandr on 31.03.15.
 */
public class StoryListFragment extends Fragment {

    private ArrayList<Story> storyList;

    @InjectView(R.id.story_list_view)
    ListView listView;
    @InjectView(android.R.id.progress)
    ProgressBar progress;

    @InjectView(R.id.adView_story)
    AdView mAdView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.stories_button);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.story_fragment, container, false);

        ButterKnife.inject(this, v);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Story story = (Story) listView.getItemAtPosition(position);

                Intent i = new Intent(getActivity(), StoryActivity.class);
                i.putExtra(ExtendedStoryFragment.EXTRA_STORY_ID, story.getId());
                startActivity(i);
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new StoryFetch().execute();
    }


    private class StoryFetch extends AsyncTask<Void, Void, ArrayList<Story>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mAdView.setVisibility(View.GONE);
            listView.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Story> doInBackground(Void... params) {
            return App.getDatabase().getStoriesTable().getStories();
        }

        @Override
        protected void onPostExecute(ArrayList<Story> stories) {
            storyList = stories;

            listView.setAdapter(new StoryAdapter(getActivity(), storyList));

            progress.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            mAdView.setVisibility(View.VISIBLE);

            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }
    }


}
