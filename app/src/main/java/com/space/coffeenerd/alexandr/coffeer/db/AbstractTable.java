package com.space.coffeenerd.alexandr.coffeer.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Alexandr on 01/04/2015.
 */
public class AbstractTable {

    protected SQLiteDatabase db;

    public AbstractTable(SQLiteDatabase db) {
        this.db = db;
    }
}
