package com.space.coffeenerd.alexandr.coffeer.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.space.coffeenerd.alexandr.coffeer.App;
import com.space.coffeenerd.alexandr.coffeer.R;
import com.space.coffeenerd.alexandr.coffeer.adapters.CoffeeRecipeAdapter;
import com.space.coffeenerd.alexandr.coffeer.interfaces.RecipeListCallback;
import com.space.coffeenerd.alexandr.coffeer.models.CoffeeRecipe;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by alexandr on 21.03.15.
 */
public class RecipeListFragment extends Fragment {

    protected RecipeListCallback callback;
    protected ArrayList<CoffeeRecipe> coffeeRecipeArrayList;

    @InjectView(R.id.recipe_list_view)
    ListView listView;

    @InjectView(android.R.id.progress)
    ProgressBar progress;

    @InjectView(R.id.adView_recipe)
    AdView mAdView;

    Button chooseFavorite;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.recipes_button);
    }

    @Override
    public void onAttach(Activity activity) {
        try {
            Log.d("onAttach() ", "it is OK");
            callback = (RecipeListCallback) activity;
        } catch (ClassCastException e) {
            Log.d("strange exception", "strange exception");
            throw new ClassCastException(activity.getClass().getSimpleName() +
                    " must implement " + RecipeListCallback.class.getSimpleName());
        }
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.recipe_fragment, container, false);

        ButterKnife.inject(this, rootView);

        View emptyView = rootView.findViewById(R.id.empty_view);
        chooseFavorite = (Button) emptyView.findViewById(R.id.choose);
        listView.setEmptyView(emptyView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CoffeeRecipe recipe = (CoffeeRecipe) listView.getItemAtPosition(position);
                callback.onRecipeClick(recipe);
            }
        });

        chooseFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.favorite_list_fragment, new RecipeListFragment())
//                        .addToBackStack(null)
                        .commit();
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new RecipesAllFetch().execute();
    }

    public void onUpdate() {
        new RecipesAllFetch().execute();
    }

    private class RecipesAllFetch extends AsyncTask<Void, Void, ArrayList<CoffeeRecipe>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listView.setVisibility(View.GONE);
            mAdView.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<CoffeeRecipe> doInBackground(Void... params) {

            return App.getDatabase().getRecipesTable().getRecipes();
        }

        @Override
        protected void onPostExecute(ArrayList<CoffeeRecipe> coffeeRecipes) {
            coffeeRecipeArrayList = coffeeRecipes;

            listView.setAdapter(new CoffeeRecipeAdapter(getActivity(), coffeeRecipeArrayList));

            progress.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            mAdView.setVisibility(View.VISIBLE);

            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);

        }
    }

}
